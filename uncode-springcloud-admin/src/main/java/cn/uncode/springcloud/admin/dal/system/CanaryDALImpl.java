package cn.uncode.springcloud.admin.dal.system;

import org.springframework.stereotype.Service;

import cn.uncode.springcloud.admin.model.system.dto.CanaryDTO;

import cn.uncode.dal.external.AbstractCommonDAL;

 /**
 * service类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-06-03
 */
@Service
public class CanaryDALImpl extends AbstractCommonDAL<CanaryDTO> implements CanaryDAL {

}