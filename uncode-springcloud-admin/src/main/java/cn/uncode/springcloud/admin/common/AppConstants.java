package cn.uncode.springcloud.admin.common;

public class AppConstants {
	
	/**
	 * 桌面图标
	 */
	public static final int DESK_ICON_MENU_TYPE_ON_DESK = 1;
	
	/**
	 * 开始菜单
	 */
	public static final int DESK_ICON_MENU_TYPE_START_MENU = 2;
	
	public static final int DESK_ICON_MENU_STATUS_VALID = 1;
	
	
	/**
	 * 网关路由状态
	 */
	public static final int GATEWAY_ROUTE_STATUS_OK = 1;
	public static final int GATEWAY_ROUTE_STATUS_UPDATE = 2;
	public static final int GATEWAY_ROUTE_STATUS_DELETE = 3;
	
}
