package cn.uncode.springcloud.utils.sensitive.pattern;

import ch.qos.logback.classic.pattern.MessageConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.uncode.springcloud.utils.sensitive.enums.KeywordTypeEnum;
import cn.uncode.springcloud.utils.sensitive.util.SensitiveInfoUtil;
import cn.uncode.springcloud.utils.string.StringUtil;

/**
 * 
 * @author juny
 * @date 2019年1月25日
 *
 */
public abstract class BaseConverter extends MessageConverter {

	/**
	 * 日志脱敏开关
	 */
    protected String converterCanRun = "false";
    /**
     * 匹配深度
     */
    protected Integer depth = 12;
    /**
     * 单条消息的最大长度，主要是message
     */
    protected Integer maxLength = 2048;
    /**
     * 日志脱敏关键字
     */
    protected static Map<String,String> keywordMap = new HashMap<>();

    @Override
    public void start() {
        List<String> options = getOptionList();
        //如果存在参数选项，则提取
        if (options != null && options.size() >= 3) {
            converterCanRun = String.valueOf(options.get(0));
            depth = Integer.valueOf(options.get(1));
            maxLength = Integer.valueOf(options.get(2));
            if (options.size() > 3){
                for (int i = 3;i < options.size();i++) {
                    String sensitiveData = String.valueOf(options.get(i));
                    if (!StringUtil.isEmpty(sensitiveData)) {
                        String[] sensitiveArray = sensitiveData.split(":");
                        if (sensitiveArray.length == 2) {
                            String keywordType = sensitiveArray[0];
                            KeywordTypeEnum keywordTypeEnum = KeywordTypeEnum.getMessageType(keywordType);
                            if (keywordTypeEnum != null && !StringUtil.isEmpty(sensitiveArray[1])) {
                                String[] keywordArray = sensitiveArray[1].split(";");
                                if (keywordArray.length > 0) {
                                    for (String keyword : keywordArray) {
                                        keywordMap.put(keyword, keywordType);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        super.start();
    }

    @Override
    public String convert(ILoggingEvent event){
        String oriLogMsg = event.getFormattedMessage();
        if (oriLogMsg == null || oriLogMsg.isEmpty()) {
            return oriLogMsg;
        }
        //如果超长截取
        if (oriLogMsg.length() > maxLength) {
            oriLogMsg = oriLogMsg.substring(0, maxLength) + "<<<";//后面增加三个终止符
        }
        return invokeMsg(oriLogMsg);
    }

    protected String facade(String msg, String key){

        String result = msg;
        KeywordTypeEnum keywordTypeEnum = KeywordTypeEnum.getMessageType(key);
        if (keywordTypeEnum == null) {
            keywordTypeEnum = KeywordTypeEnum.OTHER;
        }
        switch (keywordTypeEnum){
            case TRUE_NAME:
                result = SensitiveInfoUtil.handleTrueName(msg);break;
            case ID_CARD_NO:
                result = SensitiveInfoUtil.handleIdCardNo(msg);break;
            case BANKCARD_NO:
                result = SensitiveInfoUtil.handleBankcardNo(msg);break;
            case PHONE_NO:
                result = SensitiveInfoUtil.handlePhoneNo(msg);break;
            case OTHER:
                result = SensitiveInfoUtil.handleOther(msg);break;
        }
        return result;
    }

    public abstract String invokeMsg(final String oriMsg);
}
